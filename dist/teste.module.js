import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TesteComponent } from './teste.component';
/**
 * Adicione todos os componentes aqui para deixa-los disponivel
 */
var LISTA_DE_COMPONENTES = [
    TesteComponent,
];
/**
 * Adicione todos as importações componentes aqui para deixa-los disponivel
 */
var LISTA_DE_IMPORTACOES = [
    CommonModule,
];
var TesteComponentsModule = /** @class */ (function () {
    function TesteComponentsModule() {
    }
    TesteComponentsModule.decorators = [
        { type: NgModule, args: [{
                    imports: LISTA_DE_IMPORTACOES.slice(),
                    declarations: LISTA_DE_COMPONENTES.slice(),
                    exports: LISTA_DE_COMPONENTES.slice(),
                    entryComponents: LISTA_DE_COMPONENTES.slice(),
                    providers: []
                },] },
    ];
    /** @nocollapse */
    TesteComponentsModule.ctorParameters = function () { return []; };
    return TesteComponentsModule;
}());
export { TesteComponentsModule };
//# sourceMappingURL=teste.module.js.map