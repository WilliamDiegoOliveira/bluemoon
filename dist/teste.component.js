import { Component } from '@angular/core';
var TesteComponent = /** @class */ (function () {
    function TesteComponent() {
        this.message = "Click Me ...";
    }
    TesteComponent.prototype.onClick = function () {
        this.message = "Angular 2+ Minimal NPM Package. With external scss and html!";
    };
    TesteComponent.decorators = [
        { type: Component, args: [{
                    selector: 'teste',
                    template: "<div><h1 (click)=\"onClick()\">{{message}}</h1></div>",
                    styles: ["h1{color:red}"]
                },] },
    ];
    /** @nocollapse */
    TesteComponent.ctorParameters = function () { return []; };
    return TesteComponent;
}());
export { TesteComponent };
//# sourceMappingURL=teste.component.js.map