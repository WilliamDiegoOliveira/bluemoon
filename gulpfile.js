// Precisamos criar um gulpfile.js para dizer ao Gulp como compilar nosso aplicativo. Esta parte é necessária porque precisamos incorporar os arquivos externos nos componentes antes de publicar nosso pacote. O conteúdo é:
var gulp = require('gulp');
var embedTemplates = require('gulp-angular-embed-templates');
var inlineNg2Styles = require('gulp-inline-ng2-styles');

gulp.task('js:build', function () {
    gulp.src('src/*.ts') // also can use *.js files
        .pipe(embedTemplates({sourceType:'ts'}))
        .pipe(inlineNg2Styles({ base: '/src' }))
        .pipe(gulp.dest('./dist'));
});