import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TesteComponent } from './teste.component';


/**
 * Adicione todos os componentes aqui para deixa-los disponivel 
 */
const LISTA_DE_COMPONENTES = [
    TesteComponent,
];

/**
 * Adicione todos as importações componentes aqui para deixa-los disponivel 
 */
const LISTA_DE_IMPORTACOES = [
    CommonModule,
];

@NgModule({
    imports: [
        ...LISTA_DE_IMPORTACOES
    ],
    declarations: [
        ...LISTA_DE_COMPONENTES
    ],
    exports: [
        ...LISTA_DE_COMPONENTES
    ],
    entryComponents: [...LISTA_DE_COMPONENTES],
    providers: []
})

export class TesteComponentsModule { }

