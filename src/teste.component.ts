import { Component } from '@angular/core';

@Component({
     selector: 'teste',
     templateUrl: 'teste.component.html',
     styleUrls: ['teste.component.css']
})


export class TesteComponent {

     constructor() {
     }

     message = "Click Me ...";
     onClick() {
          this.message = "Angular 2+ Minimal NPM Package. With external scss and html!";
     }

}